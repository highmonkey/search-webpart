﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchPage.aspx.cs" Inherits="SearchStream.SearchPage" %>
<![CDATA[<%@ Register Src="~/SearchControl.ascx" TagName="SearchControl" TagPrefix="searchCtrl" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:Literal ID="SearchOutput" runat="server" />
    </div>
    </form>
</body>
</html>
