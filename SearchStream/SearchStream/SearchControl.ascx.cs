﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SearchStream
{
    public partial class SearchControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SearchOutput.Text = Execute("URL", "FunnelBackURL");
        }

        public static string Execute(string CurrentURL, string baseURL)
        {
            return GetContent(CurrentURL, baseURL);
        }

        public static string GetQuery(string CurrentURL)
        {
            string queryString = string.Empty;

            int startLength = CurrentURL.IndexOf("?");
            if (startLength != -1)
            {
                queryString = CurrentURL.Substring(startLength);
            }
            else
            {
                //put in default search query
                queryString = "?collection=HM_WebSite&query=test";
            }

            return queryString;
        }

        protected static string GetContent(string CurrentURL, string baseURL)
        {
            string contentString = "test";

            string searchURL = baseURL + GetQuery(CurrentURL);

            WebRequest request = WebRequest.Create(searchURL);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string html = String.Empty;
            using (StreamReader sr = new StreamReader(data))
            {
                html = sr.ReadToEnd();
            }

            string cleanedHTML = CleanURL(html, "");

            contentString = cleanedHTML;

            return contentString;
        }

        protected static string CleanURL(string html, string baseURL)
        {
            string cleanString = string.Empty;

            cleanString = html.Replace("search.html", baseURL);
            cleanString = cleanString.Replace("resources-global", "http://searchtest.highmonkey.com/s/resources-global");
            
            return cleanString;
        }
    }
}