﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SearchStream;

namespace SearchStream
{
    public partial class SearchPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string path = new Uri(HttpContext.Current.Request.Url.AbsoluteUri).OriginalString;
            string n = String.Format("{0}", Request.QueryString["query"]);
            SearchOutput.Text = SearchControl.Execute(path, "http://searchtest.highmonkey.com/s/search.html");
        }
    }
}